<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function task_manager_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: features:tasks
  $menu_links['features:tasks'] = array(
    'menu_name' => 'features',
    'link_path' => 'tasks',
    'router_path' => 'tasks',
    'link_title' => 'Tasks',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-10',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Tasks');


  return $menu_links;
}
