<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function task_manager_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function task_manager_node_info() {
  $items = array(
    'milestone' => array(
      'name' => t('Milestone'),
      'module' => 'features',
      'description' => t('Task Manager milestones'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'task' => array(
      'name' => t('Task'),
      'module' => 'features',
      'description' => t('Task Manager tasks'),
      'has_title' => '1',
      'title_label' => t('Task Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_defaults().
 */
function task_manager_rules_defaults() {
  return array(
    'rules' => array(
      'task_manager_10' => array(
        '#type' => 'rule',
        '#set' => 'rules_task_due_date_notify',
        '#label' => 'Email user',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(),
        '#version' => 6003,
      ),
      'task_manager_5' => array(
        '#type' => 'rule',
        '#set' => 'event_node_update',
        '#label' => 'Redirect after updating Task/Milestone',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'milestone' => 'milestone',
                'task' => 'task',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Updated content is milestone or task',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Page redirect',
              'module' => 'System',
              'eval input' => array(
                '0' => 'path',
                '1' => 'query',
                '2' => 'fragment',
              ),
            ),
            '#name' => 'rules_action_drupal_goto',
            '#settings' => array(
              'path' => 'tasks',
              'query' => '',
              'fragment' => '',
              'force' => 0,
              'immediate' => 0,
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
      ),
      'task_manager_6' => array(
        '#type' => 'rule',
        '#set' => 'rules_task_due_date_notify',
        '#label' => 'Email user',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(),
        '#version' => 6003,
      ),
      'task_manager_7' => array(
        '#type' => 'rule',
        '#set' => 'event_flag_flagged_task_status',
        '#label' => 'Task - Mark Completed',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Flagged content is task',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'task' => 'task',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'field_name' => 'field_task_whocompleted',
              '#argument map' => array(
                'node' => 'node',
              ),
              'value' => array(
                '0' => array(
                  'uid' => NULL,
                ),
              ),
              'code' => 'global $user;
return array(
0 => array(\'uid\' => $user->uid)
);',
              'vars' => array(),
            ),
            '#name' => 'content_rules_action_populate_field',
            '#info' => array(
              'label' => 'Populate flagged content\'s field \'field_task_whocompleted\'',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'eval input' => array(
                '0' => 'code',
              ),
              'module' => 'CCK',
            ),
          ),
          '1' => array(
            '#info' => array(
              'label' => 'Populate flagged content\'s field \'field_task_datecompleted\'',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'eval input' => array(
                '0' => 'code',
              ),
              'module' => 'CCK',
            ),
            '#name' => 'content_rules_action_populate_field',
            '#settings' => array(
              'field_name' => 'field_task_datecompleted',
              '#argument map' => array(
                'node' => 'node',
              ),
              'value' => array(
                '0' => array(
                  'value' => NULL,
                  'value2' => NULL,
                  'timezone' => NULL,
                  'offset' => NULL,
                  'offset2' => NULL,
                  'rrule' => NULL,
                ),
              ),
              'code' => '$today = strtotime("now");
return array(
0 => array(\'value\' => $today)
);',
              'vars' => array(),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'task_manager_8' => array(
        '#type' => 'rule',
        '#set' => 'event_flag_unflagged_task_status',
        '#label' => 'Task - Mark Not Completed',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Flagged content is task',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'task' => 'task',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'field_name' => 'field_task_whocompleted',
              '#argument map' => array(
                'node' => 'node',
              ),
              'value' => array(
                '0' => array(
                  'uid' => NULL,
                ),
              ),
              'code' => 'return array(0 => "");',
            ),
            '#name' => 'content_rules_action_populate_field',
            '#info' => array(
              'label' => 'Populate flagged content\'s field \'field_task_whocompleted\'',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'eval input' => array(
                '0' => 'code',
              ),
              'module' => 'CCK',
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Populate flagged content\'s field \'field_task_datecompleted\'',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'eval input' => array(
                '0' => 'code',
              ),
              'module' => 'CCK',
            ),
            '#name' => 'content_rules_action_populate_field',
            '#settings' => array(
              'field_name' => 'field_task_datecompleted',
              '#argument map' => array(
                'node' => 'node',
              ),
              'value' => array(
                '0' => array(
                  'value' => NULL,
                  'value2' => NULL,
                  'timezone' => NULL,
                  'offset' => NULL,
                  'offset2' => NULL,
                  'rrule' => NULL,
                ),
              ),
              'code' => 'return (array(0=>""));',
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
      ),
      'task_manager_9' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Redirect after adding New Task/Milestone',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'task_manager',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Created content is milestone or task',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'milestone' => 'milestone',
                'task' => 'task',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'path' => 'tasks',
              'query' => '',
              'fragment' => '',
              'force' => 0,
              'immediate' => 0,
            ),
            '#name' => 'rules_action_drupal_goto',
            '#info' => array(
              'label' => 'Page redirect',
              'module' => 'System',
              'eval input' => array(
                '0' => 'path',
                '1' => 'query',
                '2' => 'fragment',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
    ),
    'rule_sets' => array(
      'rules_task_due_date_notify' => array(
        'arguments' => array(
          'arg_assigned_user' => array(
            'label' => 'Assigned to user',
            'type' => 'user',
          ),
          'arg_due_date' => array(
            'label' => 'Due Date',
            'type' => 'date',
          ),
          'arg_task' => array(
            'label' => 'Task that is due',
            'type' => 'node',
          ),
        ),
        'label' => 'Ping user approaching Task Due Date',
        'status' => 'default',
        'categories' => array(
          '0' => 'task_manager',
          'task_manager' => 'task_manager',
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_views_api().
 */
function task_manager_views_api() {
  return array(
    'api' => '2',
  );
}
