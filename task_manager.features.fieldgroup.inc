<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function task_manager_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_task_completedinfo
  $groups['task-group_task_completedinfo'] = array(
    'group_type' => 'standard',
    'type_name' => 'task',
    'group_name' => 'group_task_completedinfo',
    'label' => 'Completed Info',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset_collapsed',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'label' => 'above',
      ),
    ),
    'weight' => '3',
    'fields' => array(
      '0' => 'field_task_whocompleted',
      '1' => 'field_task_datecompleted',
    ),
  );

  // Exported group: group_task_metadata
  $groups['task-group_task_metadata'] = array(
    'group_type' => 'standard',
    'type_name' => 'task',
    'group_name' => 'group_task_metadata',
    'label' => 'Additional Data',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset_collapsed',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'label' => 'above',
      ),
    ),
    'weight' => '2',
    'fields' => array(
      '0' => 'field_task_metadata',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Data');
  t('Completed Info');

  return $groups;
}
