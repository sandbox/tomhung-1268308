<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function task_manager_user_default_permissions() {
  $permissions = array();

  // Exported permission: create milestone content
  $permissions['create milestone content'] = array(
    'name' => 'create milestone content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: create task content
  $permissions['create task content'] = array(
    'name' => 'create task content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: delete any milestone content
  $permissions['delete any milestone content'] = array(
    'name' => 'delete any milestone content',
    'roles' => array(),
  );

  // Exported permission: delete any task content
  $permissions['delete any task content'] = array(
    'name' => 'delete any task content',
    'roles' => array(),
  );

  // Exported permission: delete own milestone content
  $permissions['delete own milestone content'] = array(
    'name' => 'delete own milestone content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: delete own task content
  $permissions['delete own task content'] = array(
    'name' => 'delete own task content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: edit any milestone content
  $permissions['edit any milestone content'] = array(
    'name' => 'edit any milestone content',
    'roles' => array(),
  );

  // Exported permission: edit any task content
  $permissions['edit any task content'] = array(
    'name' => 'edit any task content',
    'roles' => array(),
  );

  // Exported permission: edit own milestone content
  $permissions['edit own milestone content'] = array(
    'name' => 'edit own milestone content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: edit own task content
  $permissions['edit own task content'] = array(
    'name' => 'edit own task content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: publish any milestone content
  $permissions['publish any milestone content'] = array(
    'name' => 'publish any milestone content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: publish any task content
  $permissions['publish any task content'] = array(
    'name' => 'publish any task content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: publish own milestone content
  $permissions['publish own milestone content'] = array(
    'name' => 'publish own milestone content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: publish own task content
  $permissions['publish own task content'] = array(
    'name' => 'publish own task content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: unpublish any milestone content
  $permissions['unpublish any milestone content'] = array(
    'name' => 'unpublish any milestone content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: unpublish any task content
  $permissions['unpublish any task content'] = array(
    'name' => 'unpublish any task content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: unpublish own milestone content
  $permissions['unpublish own milestone content'] = array(
    'name' => 'unpublish own milestone content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: unpublish own task content
  $permissions['unpublish own task content'] = array(
    'name' => 'unpublish own task content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  return $permissions;
}
