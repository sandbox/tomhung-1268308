<?php

include_once('task_manager.features.inc');

/**
 * Implementation of hook_init().
 */
function task_manager_init() {
  drupal_add_css(drupal_get_path('module', 'task_manager') .'/task_manager.css');
}

/**
 * Implementation of hook_help().
 */
function task_manager_help($path, $arg) {
  if (($path == 'help#task_manager') || context_isset('context', 'task_manager')) {
    switch ($path) {
      case 'node/add/task':
      case 'node/add/milestone':
      case 'node/%/edit':
        $help = '<h3>'. t('Task Manager') .'</h3>';
        $node = menu_get_object();
        if ($path == 'node/add/milestone' || $node->type == 'milestone') {
          $help .= '<p>'. t('Tips for adding and editing milestones:') .'</p>';
          $help .= '<ul>';
          $help .= '<li>'. t('Tips here.') .'</li>';
          $help .= '</ul>';
        }
        else if ($path == 'node/add/task' || $node->type == 'task') {
          $help .= '<p>'. t('Tips for adding and editing tasks:') .'</p>';
          $help .= '<ul>';
          $help .= '<li>'. t('Tips here.') .'</li>';
          $help .= '</ul>';
        }
        return $help;

      case 'help#task_manager':
      default:
        $help = '<h3>'. t('Task Manager') .'</h3>';
        $help .= '<p>'. t('Keep your group on track by creating tasks and milestones.') .'</p>';
        $help .= '<ul>';
        $help .= '<li>'. t('!add_ms to add a milestone, or group of tasks.', array('!add_ms' => l(t('Add milestone'), 'node/add/milestone'))) .'</li>';
        $help .= '<li>'. t('!add_task to add a task to be completed.', array('!add_task' => l(t('Add task'), 'node/add/task'))) .'</li>';
        $help .= '</ul>';
        return $help;
    }
  }
}

/**
 * Implementation of hook_atrium_dashboard().
 */
function task_manager_atrium_dashboard() {
  $blocks = array();
  $blocks['views_task_manager_tasks-block_1'] = array(
    'module' => 'views',
    'delta' => 'task_manager_tasks-block_1',
    'weight' => 20,
    'region' => 'right',
  );
  return $blocks;
}

function task_manager_neat_trim($str, $n, $delim = '...') {
   $len = strlen($str);
   if ($len > $n) {
     preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
     return rtrim($matches[1]) . $delim;
   }
   else {
     return $str;
   }
}

/**
 * Implementation of hook_form_alter()
 * This uses the logged in user as the default exposed filter on the "My Tasks" view tab
 */
function task_manager_form_alter (&$form, &$form_state, $form_id) {
   if ($form['#id'] == 'views-exposed-form-task-manager-tasks-page-2') {
    global $user;
    $defaultval = $_GET['field_task_assignedto_uid'];
    if (!isset($defaultval)) {
      if ($form['field_task_assignedto_uid']['#multiple']) {
        $form_state['input']['field_task_assignedto_uid'] = array("$user->uid");
      }
      else {
        $form_state['input']['field_task_assignedto_uid'] = "$user->uid";
      }
    }
  }
  if ($form['#id'] == 'views-exposed-form-task-manager-tasks-block-1') {
    global $user;
    $form_state['input']['field_task_assignedto_uid'] = "150";
    $form_state['exposed'] = FALSE;
//    $form_state['field_task_assignedto_uid ']['#type'] = 'hidden';
//    dsm($form_state);
  }
}

/**
 * Implementation of hook_flag_default_flags()
 * http://drupal.org/node/305086#default-flags
 * This sets up the flags needed for this feature since features does not export flags
 */
function task_manager_flag_default_flags() {
  $flags = array();
  $flags[] = array(
    'content_type' => 'node',
    'name' => 'task_status',
    'title' => 'Task Status',
    'roles' => array('2'),
    'global' => TRUE,
    'types' => array('task'),
    'flag_short' => 'not completed',
    'flag_long' => 'Click to mark this task as "completed"',
    'flag_message' => '',
    'unflag_short' => 'completed',
    'unflag_long' => 'Click to mark this task as "not completed"',
    'unflag_message' => '',
    'show_on_page' => TRUE,
    'show_on_teaser' => TRUE,
    'show_on_form' => FALSE,
    'status' => TRUE,
    'locked' => array('name', 'types', 'global'),
  );
  $flags[] = array(
    'content_type' => 'node',
    'name' => 'task_archive',
    'title' => 'Task Archive',
    'roles' => array('2'),
    'global' => TRUE,
    'types' => array('task'),
    'flag_short' => 'active',
    'flag_long' => 'Click to mark this task as "archived"',
    'flag_message' => '',
    'unflag_short' => 'archive',
    'unflag_long' => 'Click to mark this task as "active"',
    'unflag_message' => '',
    'show_on_page' => TRUE,
    'show_on_teaser' => TRUE,
    'show_on_form' => FALSE,
    'status' => TRUE,
    'locked' => array('name', 'types', 'global'),
  );
  return $flags;
}

