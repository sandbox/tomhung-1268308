<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function task_manager_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'task_manager_layout';
  $context->description = '';
  $context->tag = 'Task Manager';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'milestone' => 'milestone',
        'task' => 'task',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'task_manager_tasks' => 'task_manager_tasks',
        'task_manager_tasks:page_1' => 'task_manager_tasks:page_1',
        'task_manager_tasks:page_2' => 'task_manager_tasks:page_2',
        'task_manager_tasks:page_3' => 'task_manager_tasks:page_3',
        'task_manager_tasks:page_4' => 'task_manager_tasks:page_4',
        'task_manager_tasks:page_5' => 'task_manager_tasks:page_5',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
    'menu' => 'features',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Task Manager');
  $export['task_manager_layout'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'task_manager_multi_menu';
  $context->description = '';
  $context->tag = 'Task Manager';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'manager' => 'manager',
      ),
    ),
    'views' => array(
      'values' => array(
        'task_manager_tasks' => 'task_manager_tasks',
        'task_manager_tasks:page_1' => 'task_manager_tasks:page_1',
        'task_manager_tasks:page_2' => 'task_manager_tasks:page_2',
        'task_manager_tasks:page_3' => 'task_manager_tasks:page_3',
        'task_manager_tasks:page_4' => 'task_manager_tasks:page_4',
        'task_manager_tasks:page_5' => 'task_manager_tasks:page_5',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-multi-node-add-tasks' => array(
          'module' => 'menu',
          'delta' => 'menu-multi-node-add-tasks',
          'region' => 'page_tools',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Task Manager');
  $export['task_manager_multi_menu'] = $context;

  return $export;
}
