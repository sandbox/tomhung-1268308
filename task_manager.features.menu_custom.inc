<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function task_manager_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-multi-node-add-tasks
  $menus['menu-multi-node-add-tasks'] = array(
    'menu_name' => 'menu-multi-node-add-tasks',
    'title' => 'Add Multiple',
    'description' => 'Allows adding multiple tasks or milestones',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add Multiple');
  t('Allows adding multiple tasks or milestones');


  return $menus;
}
